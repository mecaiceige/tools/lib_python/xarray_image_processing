import xarray as xr
import numpy as np

def auto_correlation(self,pad=1,method='nearest',dic=False):
    '''
    :param pad: pading between 1 and 2
    :type pad: float
    :param method: method option for xr.DataArray.sel
    :param dic: use 1-(1-Cinf)/2 for extracting with at half height for speckle caracteriastion
    :type dic: bool
    '''
    data=np.array(self)
    # Compute mean for replacing nan value and for padding
    mean_data=np.nanmean(data)
    # Replace nan value
    id=np.where(np.isnan(data))
    data[id]=mean_data
    if pad >1:
        ss=data.shape
        fpad=np.ones([pad*ss[0],pad*ss[1]])*mean_data
        fpad[0:ss[0],0:ss[1]]=data
    else:
        fpad=data

    # Compute autocorrelation
    FFT_fpad=np.fft.fft2(fpad)
    abs_FFTpad=np.abs(FFT_fpad)**2
    An=np.fft.ifft2(abs_FFTpad)
    mAn=np.nanmax(An)

    Autocor=np.abs(np.fft.fftshift(An/mAn));
    Cinf=mean_data**2/np.mean(fpad**2);
    res=xr.DataArray(Autocor,dims=('ya','xa'))
    ssa=Autocor.shape
    d_dims=self.dims
    res.coords['ya']=np.array(np.abs(self[d_dims[1]][1]-self[d_dims[1]][0]))*np.linspace(0,ssa[0]-1,ssa[0])
    res.coords['xa']=np.array(np.abs(self[d_dims[0]][1]-self[d_dims[0]][0]))*np.linspace(0,ssa[1]-1,ssa[1])


    # etract min an max direction
    ss=res.shape

    x0=ss[1]/2*res.xa[1]
    y0=ss[0]/2*res.ya[1]
    cross=np.ones(180)

    ds=xr.Dataset()

    ds['AutoCorrelation']=res
    if dic:
        ds['Cinf']=(1+Cinf)/2
    else:
        ds['Cinf']=Cinf

    lmax=np.zeros(180)
    lmax_int=np.zeros(180)

    for i in list(range(180)):
        xt=x0+np.cos(i*np.pi/180.)*x0
        yt=y0+np.sin(i*np.pi/180.)*y0

        nb=int(((y0-yt)**2+(x0-xt)**2)**0.5/np.abs(self[d_dims[0]][1]-self[d_dims[0]][0]))
        xx=xr.DataArray(np.linspace(x0,xt,nb), dims="d"+str(i))
        yy=xr.DataArray(np.linspace(y0,yt,nb), dims="d"+str(i))

        profil=res.sel(xa=xx,ya=yy, method=method,drop=True)
        d=((xx-xx[0])**2+(yy-yy[0])**2)**0.5
        profil.coords['d'+str(i)]=d
        profil.attrs['angle']=i

        ds['P'+str(i)]=profil

        id=np.where((ds['P'+str(i)]>ds.Cinf)==False)[0]
        if np.size(id)==0:
            lmax[i]=np.nan
        else:
            lmax[i]=ds['d'+str(i)][id[0]]
            
        A1=(profil-ds.Cinf).integrate(profil.dims[0])
        lmax_int[i]=np.array(2*A1/(1-ds.Cinf))

    dlmax=xr.DataArray(lmax,dims='angle')
    dlmax_int=xr.DataArray(lmax_int,dims='angle')
    dlmax.coords['angle']=np.linspace(0,179,180)
    dlmax.attrs['unit_angle']='degree'

    ds['lmax']=dlmax
    ds['lmax_int']=dlmax_int
    return ds
