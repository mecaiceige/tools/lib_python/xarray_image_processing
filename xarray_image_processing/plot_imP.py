# This file contained plot function for image processing output.
import matplotlib.patches as mpt
from windrose import WindroseAxes
import matplotlib.pyplot as plt
import xarray as xr
import numpy as np

def plot_texture_anisotropy(data,zoom_f=1,lim_ani=0,add_box=False):
    '''
    Plot ellipsoide on image + rosewind diagram
    '''

    ll_data=data['ellipse']
    ll_image=data['image']
    rose_data=data['rose_data']
    
    fig, ax = plt.subplots(nrows=1,ncols=2,figsize=(20,10))
    ax[0].axis('off')
    ax[1].axis('off')
    
    ax[1] = fig.add_subplot(122, projection="windrose")
    str=['0','45','90','-45','0','45','-90','-45']
    ax[1].set_xticklabels(str)
   

    plt.axes(ax[0])
    ll_image.plot()
    
    patches=[]
    for i in range(len(ll_data)):
        c=ll_data[i].center[::-1]
        w=ll_data[i].a1/zoom_f
        h=ll_data[i].a2/zoom_f
        a=ll_data[i].angle*180/np.pi
        patches.append(mpt.Ellipse(xy=c,width=w,height=h,angle=a,edgecolor='w', fc='None', lw=2))
        if add_box:
            patches.append(mpt.Rectangle(xy=c-ll_data[i].box_size/2.,width=ll_data[i].box_size,height=ll_data[i].box_size,edgecolor='k', fc='None', lw=2))
            
    for p in patches:
        ax[0].add_patch(p)
    plt.axis('equal')
    
    
    wd=np.mod(360-rose_data[:,0]*180/np.pi+90,360)
    ws=rose_data[:,1]
    wd=np.concatenate([wd,np.mod(wd+180,360)])
    ws=np.concatenate([ws,ws])
    
    ax[1].bar(wd, ws, normed=True, opening=0.7, edgecolor='white',bins=[0.0,0.2,0.4,0.6,0.8] )
    ax[1].set_legend()
    
    
def plot_texture_anisotropy_2(data,zoom_f=1,add_ellipse=True,add_box=False,image=False):
    '''
    Plot ellipsoide on image + rosewind diagram
    '''
    box_size=data.box_size

    fig, ax = plt.subplots(nrows=1,ncols=2,figsize=(20,10))
    ax[0].axis('off')
    ax[1].axis('off')

    ax[1] = fig.add_subplot(122, projection="windrose")
    str=['0','45','90','-45','0','45','-90','-45']
    ax[1].set_xticklabels(str)


    plt.axes(ax[0])
    if type(image)==bool:
        data.anisotropy.plot()
        offset=0
    else:
        image.plot()
        offset=box_size/2.
        
    ss=np.shape(data.anisotropy)
    coordinate=data.anisotropy.coords.dims

    sign0=np.sign(data[coordinate[0]][1]-data[coordinate[0]][0])
    sign1=np.sign(data[coordinate[1]][1]-data[coordinate[1]][0])
    
    patches=[]
    if add_ellipse:
        for i in range(ss[0]):
            for j in range(ss[1]):
                c=[float(data[coordinate[1]][j])+sign1*offset,float(data[coordinate[0]][i])+sign0*offset]
                w=np.array(data.a1[i,j])/zoom_f
                h=np.array(data.a2[i,j])/zoom_f
                a=np.array(data.angle[i,j])*180/np.pi

                if ~np.isnan(w):
                    patches.append(mpt.Ellipse(xy=c,width=w,height=h,angle=a,edgecolor='w', fc='None', lw=2))
                    if add_box:
                        patches.append(mpt.Rectangle(xy=c-box_size/2.,width=box_size,height=box_size,edgecolor='k', fc='None', lw=2))

    for p in patches:
        ax[0].add_patch(p)
    plt.axis('equal')


    wd=np.mod(360-np.array(data.angle)*180/np.pi+90,360).flatten()
    ws=np.array(data.anisotropy).flatten()
    
    wd = wd[~(np.isnan(ws))]
    ws = ws[~(np.isnan(ws))]
    
    
    wd=np.concatenate([wd,np.mod(wd+180,360)])
    ws=np.concatenate([ws,ws])

        
    ax[1].bar(wd, ws, normed=True, opening=0.7, edgecolor='white',bins=[0.0,0.2,0.4,0.6,0.8] )
    ax[1].set_legend()